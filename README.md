# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Userstory ###

 Zaproponuj funkcjonalność programu do obsługi zawodów pucharu świata w skokach narciarskich. 
 Zapewnić: - możliwość zapisywania zawodników (wprowadzić stosowne górne ograniczenie max>50), 
 - możliwość przeprowadzenia serii eliminacyjnej 
 – punktacja sędziów: P punktów oraz za każdy metr powyżej punktu krytycznego K: plus S punktów,
 za każdy metr poniżej punktu krytycznego K: minus S punktów,
 za styl: trzy z pięciu ocen O1..O5, które pozostaną po odrzuceniu dwóch ocen o wartościach skrajnych.
 - wyłonienie 50. najlepszych zawodników, - po 1. seria skoków – wyłonienie 30. najlepszych zawodników do 2. serii,  
 - po 2. seria skoków – uporządkowanie końcowe. 
 Wprowadź stosowne dodatkowe założenia (w tym: ograniczenia). 
 

### How do I get set up? ###

git clone with Intellij idea
Java 1.8 required  

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact