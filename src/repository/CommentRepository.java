package repository;

import Entities.Comment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CommentRepository {
    private static CommentRepository instance;
    private List<Comment> Comments;
    private CommentRepository(){
        InputStream in = CommentRepository.class.getResourceAsStream("/comments/repository.json");
        JsonReader reader = new JsonReader(new InputStreamReader(in));
        Type listType = new TypeToken<ArrayList<Comment>>(){}.getType();
         Comments = new Gson().fromJson(reader, listType);
    }

    public static List<Comment> GetComments(){
        if (instance == null)
            instance = new CommentRepository();
        return instance.Comments;
    }
}
