package repository;

import Entities.Skier;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SkierRepository {
    private static final String DataSource = "https://www.ii.pwr.edu.pl/pl/pracownicy/pracownicy-naukowi-i-dydaktyczni/";
    private static List<Skier> _skierRepository;

    public List<Skier> GetSkiers(){
        if (_skierRepository != null)
            return _skierRepository;
        _skierRepository = new ArrayList<Skier>();
        Document dataSourceDoc;
        try {
             dataSourceDoc = Jsoup.connect(DataSource).get();
        } catch (IOException ex){
            return null;
        }
        Elements rawData = dataSourceDoc.select("table.pracownicy tr");

        for (int i=1; i< rawData.size();i++) {
            Skier skier = SkierFactory.Create(rawData.get(i));
            if (skier != null)
                _skierRepository.add(skier);
        }
        return _skierRepository;
        }
    }

