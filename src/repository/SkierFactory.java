package repository;

import Entities.Skier;
import org.jsoup.nodes.Element;

 class SkierFactory {
     static  int id;

    public static Skier Create(Element row){
        Element firstNameData =row.selectFirst("td:nth-child(3)");
        Element lastNameNameData = row.selectFirst("td:nth-child(2)");

        String firstName = ExtractData(firstNameData);
        String lastName = ExtractData(lastNameNameData);

        if (firstName == null || lastName == null)
            return  null;
        return  new Skier(id++,
                CapitalizeString(firstName),
                CapitalizeString(lastName));
    }

    static String CapitalizeString(String source){
        String result = source.substring(0, 1).toUpperCase() + source.substring(1).toLowerCase();
        return result;
    }

    static String ExtractData(Element elem){
        if (elem == null)
            return  null;
        Element result = elem.selectFirst("span");
        if (result == null)
            result = elem.selectFirst("a");


        String sourceStr =result ==null?elem.text().trim(): result.text().trim();
        if( !sourceStr.isEmpty())
            return sourceStr;
        return null;


    }

}
