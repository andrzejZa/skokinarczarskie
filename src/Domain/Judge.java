package Domain;

import Entities.JumpResult;
//Wisytator?
//Faktycznie, bez interfejsów
public class Judge {
    //ustawimy: linia K 50m
    //przekroczenie: +5p /m
    //inaczej -3p /m
    //ocena za styl: max 20p

    public static final double LiniaK = 50;
    private final int PositivePunkt = 5;
    private final int NegativePunkt = 3;
    public int EstimateResult(double distance) {
        // P punktów oraz za każdy metr powyżej punktu krytycznego K
        //S punktów, za każdy metr poniżej punktu krytycznego K
        // punkty za styl
        int delta =(int) (distance-LiniaK);
        int punkty = delta>0?delta*PositivePunkt:delta*NegativePunkt;
        int style = (int)(Math.random()*40)-20;
        return Math.max(0,(int)distance + punkty+style);
    }
}
