package Domain;

import Entities.IJumpResult;
import Entities.JumpResult;
import Entities.Skier;
import repository.CommentRepository;

import java.util.ArrayList;
import java.util.List;

public class Round {

    List<IJumpResult> results;
    List<Skier> skiers;
    IResultsFilter resultsFilter;
    Commentator commentator;

    public Round(List<Skier> skiers,IResultsFilter filter,String roundName){
        results = new ArrayList<>();
        this.skiers = skiers;
        resultsFilter = filter;

        commentator = new Commentator(roundName);
    }



  public  List<Skier> Start() {

       for (Skier skier : skiers) {
           results.add(commentator.PrintJumpResult(skier.DoJump(new JumpResult())));
       }
       if (resultsFilter == null)
           return skiers;
       return resultsFilter.EliminateResults(results);
   }
public void  PrintRoundResults(){

        ResultPrinter printer = new ResultPrinter();
    printer.PrintHeader(new String[]{"Miejsce","Narczarz","Długość skoka","Ocena 1","Ocena 2","Ocena 3","Ocena 4","Ocena 5"});
        for(IJumpResult skierResult:results){
            printer.AddRoundRow(skierResult);
        }
    printer.Print();

}

}


