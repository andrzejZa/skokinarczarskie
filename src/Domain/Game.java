package Domain;

import Entities.Skier;
import repository.SkierRepository;

import java.util.List;

public class Game {
    List<Skier> skiers;
    public void Start(){
        SkierRepository repository = new SkierRepository();

        Registration registration = new Registration(repository.GetSkiers(),50);
        List<Skier> qualifyingRoundResults = registration.Start();
//        Round qualifyingRound = new Round(repository.GetSkiers(),new ResultsFilter(50),"runda kwalifikacyjna");
//        List<Skier> qualifyingRoundResults = qualifyingRound.Start();

        Round round1 = new Round(qualifyingRoundResults,new ResultsFilter(30),"I runda");
        List<Skier> round1Results = round1.Start();
        round1.PrintRoundResults();

        Round round2 = new Round(round1Results,new ResultsFilter(30),"II runda");
        List<Skier> round2Results = round2.Start();
        round2.PrintRoundResults();
    }
}

