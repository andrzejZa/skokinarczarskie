package Domain;

import Entities.Comment;
import Entities.CommentType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentProcessor {
    private Map<CommentType,List<Comment>> commentList;
    private Map<CommentType,List<Comment>> commentListBackup;
    private  final String defaultComment = "[skier] Wynik: [distance]";
    public CommentProcessor(List<Comment> comments){
        commentList = new HashMap<CommentType,List<Comment>>();
        List<Comment> _comments = new ArrayList<>(comments);
        for (Comment comment:_comments){
            if (!commentList.containsKey(comment.Type))
                commentList.put(comment.Type, new ArrayList<>());
            commentList.get(comment.Type).add(comment);
        }
        commentListBackup = new HashMap<CommentType,List<Comment>>();

        for (CommentType commentType: commentList.keySet()){
            commentListBackup.put(commentType,new ArrayList<>(commentList.get(commentType)));
        }
    }

    public String Get(CommentType commentType){
        if (!commentList.containsKey(commentType))
            commentList.put(commentType, new ArrayList<>());
        if (commentList.get(commentType).size() ==0 && commentListBackup.containsKey(commentType)){
            commentList.get(commentType).addAll(new ArrayList<>(commentListBackup.get(commentType)));
        }

        if (commentList.get(commentType).size() ==0) {
            return defaultComment;
        }

        int randomIndex = (int)(commentList.get(commentType).size()*Math.random());
        Comment comment = commentList.get(commentType).get(randomIndex);
        commentList.get(commentType).remove(comment);
        return comment.Comment;
    }
}
