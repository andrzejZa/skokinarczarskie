package Domain;

import Entities.IJumpResult;
import Entities.JumpResult;
import Entities.Skier;

import java.util.List;

interface IResultsFilter {

     List<Skier> EliminateResults(List<IJumpResult> results);

}
