package Domain;

import Entities.JumpResult;
import Entities.Skier;
import Util.AsciiTable;
import repository.CommentRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Registration {

    List<Skier> results;
    List<Skier> skiers;
    int maxResult;
    public Registration(List<Skier> skiers, int maxResult){
        results = new ArrayList<>();
        this.skiers = skiers;
        this.maxResult = maxResult;

    }

    public  List<Skier> Start() {

        Collections.sort(skiers);
        Collections.reverse(skiers);
        ResultPrinter printer = new ResultPrinter();
        System.out.println("Zarejestrowane narczarzy:");
        printer.PrintHeader(new String[]{"","Narczarz","Ocena"});
        for (int i=0; i<Math.min(skiers.size(),maxResult);i++){
            results.add(skiers.get(i));
            printer.AddRow(skiers.get(i));
        }

        printer.Print();
        return results;
    }
}
