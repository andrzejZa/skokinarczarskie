package Domain;

import Entities.*;
import repository.CommentRepository;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Commentator {
    private  double minResult =1000;
    private double maxResult =0;
    private CommentProcessor commentProcessor;
    DecimalFormat df2 = new DecimalFormat(".#");
    public Commentator( String roundName) {
        commentProcessor =  new CommentProcessor(CommentRepository.GetComments());
        PrintRoundName(roundName);
    }

    private void PrintRoundName(String roundName) {
        System.out.println(roundName+" start:");
    }

    public IJumpResult PrintJumpResult(IJumpResult result){

        String estimateResult="";
        if (result.GetJumpDistance()!=0 && result.GetJumpDistance()<Judge.LiniaK && result.GetJumpDistance()<minResult) {
            estimateResult = " i jest najgorszy";
            minResult = result.GetJumpDistance();
        }

        else if(result.GetJumpDistance()>maxResult){
            estimateResult = " i jest najłiepszy";
            maxResult = result.GetJumpDistance();
        }
            System.out.println(formatter(getComment(result.IsFatal(), result.GetJumpDistance())+estimateResult,
                    result.GetSkier().FirstName+ " "+result.GetSkier().LastName,df2.format(result.GetJumpDistance())));


        return result;
    }

    private String getComment(boolean isFatal, double jumpDistance){
                if (isFatal)
                    return  commentProcessor.Get(CommentType.fatal);
                if (jumpDistance < Judge.LiniaK )
                    return  commentProcessor.Get(CommentType.bad);
        return  commentProcessor.Get(CommentType.good);



    }

    private String formatter(String message, String skier, String distance) {
       return message.replace("[skier]",skier).replace("[distance]",distance);

    }
}
