package Domain;

import Entities.IJumpResult;
import Entities.JumpResult;
import Entities.Skier;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

public class ResultsFilter implements IResultsFilter {
    int maxResult;
    @Override
    public List<Skier> EliminateResults(List<IJumpResult> results) {
        List<Skier> resultFilter = new ArrayList<>();
        Collections.sort(results);
        Collections.reverse(results);

        for (int i=0; i<Math.min(maxResult,results.size());i++){
            if (!results.get(i).IsFatal())
            resultFilter.add(results.get(i).GetSkier());
        }
        return resultFilter;

    }

    public ResultsFilter(int maxResult) {
        this.maxResult = maxResult;

    }
}
