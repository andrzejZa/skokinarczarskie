package Domain;

import Entities.IJumpResult;
import Entities.JumpResult;
import Entities.Skier;
import Util.AsciiTable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ResultPrinter {
    private AsciiTable t;
    private List<String[]> table;

    public ResultPrinter() {
        t = new AsciiTable();
        table = new ArrayList<>();
    }

    public void PrintHeader(String[] header)
    {
        table.add(header);
    }

    public void PrintData(List<Skier> skiers){

    }

    public void AddRow(Skier skier){
        table.add(new String[]{table.size()+"",skier.FirstName+" "+skier.LastName, skier.Experience+""});
    }
    public void AddRoundRow(IJumpResult result) {
         DecimalFormat df2 = new DecimalFormat(".##");
        String jumpDistance = result.GetJumpDistance() ==0?0+"":df2.format(result.GetJumpDistance());
        table.add(new String[]{table.size()+"",result.GetSkier().FirstName+" "+result.GetSkier().LastName,jumpDistance+"",
                result.GetEstimates()[0]+"", result.GetEstimates()[1]+"", result.GetEstimates()[2]+"",
                result.GetEstimates()[3]+ "",result.GetEstimates()[4]+""});
    }

    public void Print(){
        t.setBorderLessTable(true);
        t.setColumnAlignment(1,AsciiTable.ALIGN_LEFT);
        t.setColumnAlignment(2,AsciiTable.ALIGN_LEFT);
        t.adjustColumnSize(table);
        t.render(table);
    }
}
