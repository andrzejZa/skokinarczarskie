package Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AsciiTable implements Serializable {



    /**
     * A tool to render datas into an ASCII table to the console.<br>
     * By default, columns are 10 characters length.<br>
     * The column size means data size into it. Marges are excluded.<br>
     * Marges are fixed to 1 blank character. One before the data (after the column separator character) and one after the
     * data (before the column separator character).<br>
     * Example :<br>
     * <code>| dataValue    |</code><br>
     * In this example, data value is set to "dataValue" and column si is set to 10 characters (default).<br>
     * The two '|' characters are the columns separators.<br>
     * There is two margin blank characters, one after and one before column separators.<br>
     * <br>
     * If the data value size exceeds the column size, data value is truncated by its right.<br>
     * Example :<br>
     * <code>| dataV |</code><br>
     *  In this example, data value is set to "dataValue" (the same as precedent example), but column size is set
     *  to 5. The first 5 characters "dataV" are printed and the rest "alue" is truncated.<br>
     *  It is possible to modify the default column size - 10 by default - by calling the setColumnSize method
     *  with one integer argument.<br>
     *  It is also possible to define one ore much particular column size by calling the setColulnSize method
     *  with two integer arguments.<br>
     *  <br>
     *  Data values can be rendered as <code>String[][]</code> or <code>List<List<String>></code><br>
     *  Each render method is optimized for its argument type (array or list)<br>
     *  <br>
     *  By default, null values are renderer with an empty String "".<br>
     *  You can modify that by call setNullValueString method.
     *
     * @author Val�re Bertin-Sanc:hez
     * @version 1.0
     */


    private static final long serialVersionUID = -944001835506823470L;

    public static final int ALIGN_LEFT=0;
    public static final int ALIGN_CENTER=1;
    public static final int ALIGN_RIGHT=2;

    private Map<Integer, Integer> columnSizes;
    private int defaultColumnSize;
    private String nullValueString;

    private int defaultAlignment = AsciiTable.ALIGN_CENTER;
    private Map<Integer, Integer> columnAlignments;

    private Boolean borderLessTable;
    private Boolean printRowNumber;
    /**
     * Create a new rendering table.<br>
     * Column are 10 characters length by default.
     */
    public AsciiTable(){
        this.columnSizes = new HashMap<Integer, Integer>();
        this.defaultColumnSize = 10;
        this.nullValueString = "";

        this.columnAlignments = new HashMap<Integer, Integer>();
    }

    /**
     * Get column alignment.
     * @param columnIndex Column index. Indexes start at 0.
     * @return Column alignment.
     */
    public int getColumnAlignment(int columnIndex){
        Integer align = this.columnAlignments.get(columnIndex);
        if( align == null ){
            return this.defaultAlignment;
        }else{
            return align;
        }
    }

    /**
     * Set alignment for specified column.
     * @param columnIndex Column index. Indexes start at 0.
     * @param alignment Column alignment.
     */
    public void setColumnAlignment(int columnIndex, int alignment){
        this.columnAlignments.put(columnIndex, alignment);
    }

    /**
     * Set column aligment.
     * @param alignment Column alignment.
     */
    public void setColumnAlignment(int alignment){
        this.defaultAlignment = alignment;
    }

    public void setBorderLessTable(Boolean borderLessTable){
        this.borderLessTable = borderLessTable;
    }

    public void setPrintRowNumber(Boolean printRowNumber) {
        this.printRowNumber = printRowNumber;
    }

    /**
     * @return The null value string used for rendering.
     */
    public String getNullValueString() {
        return nullValueString;
    }

    /**
     * Align the specified data.
     * @param data Data to align.
     * @param size Size of column for alignment.
     * @param alignment Alignment type.
     * @return Aligned data.
     */
    private static String align(String data, int size, int alignment){
        int align = alignment;
        if( align < 0 || align > 2 ) align = AsciiTable.ALIGN_CENTER;
        if( data.length() >= size ) return data.substring(0,size);
        StringBuffer sb = new StringBuffer(size);
        switch(align){
            case AsciiTable.ALIGN_LEFT:
                sb.append(data);
                while( sb.length() < size) sb.append(" ");
                break;
            case AsciiTable.ALIGN_RIGHT:
                int blankCpt = size - data.length();
                for(int i=0; i<blankCpt; i++){
                    sb.append(" ");
                }
                sb.append(data);
                break;
            case AsciiTable.ALIGN_CENTER:
                blankCpt = (size - data.length()) / 2;
                for(int i=0; i<blankCpt; i++){
                    sb.append(" ");
                }
                sb.append(data);
                for(int i=0; i<blankCpt; i++){
                    sb.append(" ");
                }
                if( sb.length() < size ) sb.append(" ");
                break;
        }
        return sb.toString();
    }

    /**
     * Setting the null value string rendering.
     * @param nullValueString
     */
    public void setNullValueString(String nullValueString) {
        this.nullValueString = nullValueString;
    }

    /**
     * Computing the maximum column for a row from datas.
     * @param datas Datas to analyse.
     * @return Max column count.
     */



    private static int getMaxColumns(List<String[]> datas){
        if( datas != null ){
            int max = 0;
            for(String[] line : datas){
                if( line != null && line.length > max ) max = line.length;
            }
            return  max;
        }
        return 0;
    }

    /**
     * Computing the maximum column for a row from datas.
     * @param datas Datas to analyse.
     * @return Max column count.
     */
    private static int getMaxColumns(String [][] datas){
        if( datas != null ){
            int max = 0;
            for(String [] line : datas){
                if( line != null && line.length > max ) max = line.length;
            }
            return max;
        }
        return 0;
    }

    /**
     * Get size of the specified column.
     * @param columnIndex Column index. Indexes start at 0.
     * @return Column size.
     */
    public int getColumnSize(int columnIndex){
        Integer size = this.columnSizes.get(columnIndex);
        if( size == null ){
            return this.defaultColumnSize;
        }else{
            return size;
        }
    }

    /**
     * Compute the maximum line size from datas.<br>
     * A line size counts :<br>
     * - Column separator characters.<br>
     * - Margin blank characters.<br>
     * - Data values characters extended or truncated to each column size.
     * @param datas Datas to analyse.
     * @return Max line size.
     */

    private int getMaxLineSize(List<String[]> datas){
        int maxColumns = AsciiTable.getMaxColumns(datas);
        int maxSize = 1 + ( 3 * maxColumns);
        for(int i=0; i<maxColumns; i++){
            maxSize += this.getColumnSize(i);
        }
        return maxSize;
    }
    /**
     * Compute the maximum line size from datas.<br>
     * A line size counts :<br>
     * - Column separator characters.<br>
     * - Margin blank characters.<br>
     * - Data values characters extended or truncated to each column size.<br>
     * @param datas Datas to analyse.
     * @return Max line size.
     */
    private int getMaxLineSize(String [][] datas){
        int maxColumns = AsciiTable.getMaxColumns(datas);
        int maxSize = 1 + ( 3 * maxColumns);
        for(int i=0; i<maxColumns; i++){
            maxSize += this.getColumnSize(i);
        }
        return maxSize;
    }

    /**
     * Compute an outer separator line (before first and after end data lines)
     * @param datas Datas
     * @return An outer separator line.
     */


    private String getOuterSparatorLine(List<String[]> datas){
        int maxLineSize = this.getMaxLineSize(datas);
        StringBuffer sb = new StringBuffer(maxLineSize);
        for(int i=0; i<maxLineSize; i++){
            sb.append('-');
        }
        return sb.toString();
    }

    /**
     * Compute an outer separator line (before first and after end data lines)
     * @param datas Datas
     * @return An outer separator line.
     */
    private String getOuterSparatorLine(String [][] datas){
        int maxLineSize = this.getMaxLineSize(datas);
        StringBuffer sb = new StringBuffer(maxLineSize);
        for(int i=0; i<maxLineSize; i++){
            sb.append('-');
        }
        return sb.toString();
    }

    /**
     * Render datas as ASCII table to the console.
     * @param datas Datas.
     */

    public void adjustColumnSize(List<String[]> datas){
        for (int i=0;i<datas.size();i++){
            for (int j=0;j<datas.get(i).length;j++){
                if (datas.get(i)[j].length()>getColumnSize(j))
                    setColumnSize(j,datas.get(i)[j].length());
            }
        }
    }

    public void render(List<String[]> datas){
        String outerSeparayorLine = this.getOuterSparatorLine(datas);
        int maxColumns = AsciiTable.getMaxColumns(datas);

        System.out.println(outerSeparayorLine);
        for(int i=0; i<datas.size(); i++){
            System.out.print('|');
            String[] row = datas.get(i);
            if( row == null ){
                row = new String[maxColumns];
                for(int j=0; j<maxColumns; j++){
                    row[j] = this.nullValueString;
                }
            }

            for(int j=0; j<maxColumns; j++){
                String data;
                if( j>=row.length || row[j]== null){
                    data=this.nullValueString;
                }else{
                    data = row[j];
                }
                int columnSize = getColumnSize(j);
                String alignedData = AsciiTable.align(data, columnSize, this.getColumnAlignment(j));
                System.out.print(" " + alignedData + " |");
            }
            System.out.println();
            if( i <datas.size() - 1 && !(borderLessTable && i>0)) System.out.println(outerSeparayorLine);
        }
        System.out.println(outerSeparayorLine);
    }

    /**
     * Render datas as ASCII table to the console.
     * @param datas Datas.
     */
    public void render(String [][] datas){
        String outerSeparayorLine = this.getOuterSparatorLine(datas);
        int maxColumns = AsciiTable.getMaxColumns(datas);

        System.out.println(outerSeparayorLine);
        for(int i=0; i<datas.length; i++){
            System.out.print('|');
            String [] row = datas[i];
            if( row == null ){
                row = new String[maxColumns];
                for(int j=0; j<maxColumns; j++){
                    row[j]=this.nullValueString;
                }
            }
            for(int j=0; j<maxColumns; j++){
                String data;
                if( j>=row.length || row[j] == null){
                    data=this.nullValueString;
                }else{
                    data = row[j];
                }
                int columnSize = getColumnSize(j);
                String alignedData = AsciiTable.align(data, columnSize, this.getColumnAlignment(j));
                System.out.print(" " + alignedData + " |");
            }
            System.out.println();
            if( i <datas.length - 1) System.out.println(outerSeparayorLine);
        }
        System.out.println(outerSeparayorLine);
    }

    /**
     * Set the default column size. All columns are concerned except particular column size set by
     * the setColumnSize two-charactered method.
     * @param defaultColumnSize Size of data values printed.
     */
    public void setColumnSize(int defaultColumnSize) {
        this.defaultColumnSize = defaultColumnSize;
    }

    /**
     * Modify a particular column size.
     * @param columnIndex Index of column. Indexes start at 0.
     * @param columnSize The size of data value printed.
     */
    public void setColumnSize(int columnIndex, int columnSize){
        this.columnSizes.put(columnIndex, columnSize);
    }
}


