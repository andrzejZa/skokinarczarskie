package com.company;

import Domain.Game;
import Domain.GameHelper;
import Domain.ResultsFilter;
import Domain.Round;
import Entities.Skier;
import repository.SkierRepository;

import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Game game = new Game();
        game.Start();
    }
}
