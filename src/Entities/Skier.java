package Entities;

import java.util.ArrayList;
import java.util.List;

public class Skier implements Comparable<Skier>, IJump{
    public int Id;
    public String FirstName;
    public String LastName;
    public int Experience;

    public List<JumpResult> results;
    public Skier(int id, String firstName, String lastName){
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        results = new ArrayList<>();
        Experience = (int)(Math.random()*100);
    }

    @Override
    public int compareTo(Skier o) {
        return Experience>o.Experience?1:-1;
    }

    @Override
    public JumpResult DoJump(IJumpVisitor jump) {
        JumpResult result = jump.DoJump(this);
        results.add(result);
        return  result;

    }
}
