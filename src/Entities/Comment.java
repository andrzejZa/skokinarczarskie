package Entities;

import java.io.Serializable;

public class Comment implements Serializable {
    public CommentType Type;
    public String Comment;

    public Comment(CommentType type, String comment) {
        Type = type;
        Comment = comment;
    }
}
