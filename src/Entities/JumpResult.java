package Entities;

import Domain.Judge;

import java.util.Arrays;

public class JumpResult implements IJumpVisitor,IJumpResult{
    private Skier skier;
    private double JumpDistance;
    private boolean IsFatal;
    private int[] Estimates;
    public JumpResult(){

    }

    @Override
    public Skier GetSkier() {
        return skier;
    }

    @Override
    public boolean IsFatal() {
        return IsFatal;
    }

    @Override
    public double GetJumpDistance() {
        return JumpDistance;
    }

    @Override
    public  JumpResult DoJump(Skier skier) {
        this.skier = skier;
        double randomizer =  Math.random()+Math.random()+Math.random();
        IsFatal = randomizer/3 - skier.Experience/100 <0.2;
        JumpDistance = IsFatal?0:Math.random()*100;
        Estimates = InitEstimates();
        return this;
    }

    @Override
    public int compareTo(IJumpResult o) {
        if (GetJudgeResults() == o.GetJudgeResults())
            return GetJumpDistance() -o.GetJumpDistance()>0?1:-1;
       return GetJudgeResults() - o.GetJudgeResults()>0?1:-1;

    }

    private  int[] InitEstimates(){
        int[] result = new int[5];
        for (int i=0;i<5;i++){
            Judge judge = new Judge();
            result[i] = judge.EstimateResult(JumpDistance);
        }
        return result;
    }

    public int[] GetEstimates(){
        if (Estimates == null)
            Estimates = InitEstimates();
        return Estimates;

    }

    public int GetJudgeResults(){
        int result=0;
        if (Estimates == null)
            return result;
        Arrays.sort(Estimates);
        for (int i=0;i<4;i++)
            result+=Estimates[i];
        return result;
    }
}

 interface  IJumpVisitor{
    JumpResult DoJump(Skier skier);

}