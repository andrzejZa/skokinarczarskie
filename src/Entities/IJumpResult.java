package Entities;

public interface IJumpResult extends Comparable<IJumpResult> {
    public Skier GetSkier();
    public double GetJumpDistance();
    public int[] GetEstimates();
    public int GetJudgeResults();
    public boolean IsFatal();
}
